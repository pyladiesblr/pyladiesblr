Title: PyLadies Bengaluru Announcement
Date: 2018-12-28 22:26
Category: Who are we?
Tags: pyladies, python
Slug: pyladies-bengaluru
Authors: Aishwarya Padmanabha, Pooja Gadige, Tanvi Bhakta

PyLadies is an International mentorship group that helps, encourages and supports more women become active participants and leaders in the Python Open Source community. PyLadies Bengaluru is the group with an emphasis on promoting diverse, inclusive, empathetic Python community for women and underrepresented minorities in the city. We aim to provide a friendly support network and a bridge to the larger Python world through outreach, educating and encouraging women and underrepresented minorities contribute to open source community in all forms. This group was started with the aim of reaching out to all these amazing women in tech and help gain confidence in all forms, irrespective of how experienced you may be in the field.
