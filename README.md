# pyladiesblr
Repository for the pyladiesblr website

# Getting started

Install Dependencies

1. Create a [virtual environment](https://docs.python.org/3/tutorial/venv.html)
1. `pip install -r requirements.txt`

To build the project:

1. ` make html`
1. `make serve [PORT=8000]`

# Options

`make help`

# Framework

[Pelican](https://github.com/getpelican/pelican)

# Contributing code

Before you submit a contribution, please discuss with us, so that you don't spend a lot of time working on something that would be rejected for a known reason.

1. If you would like to file an issue, first check for existing [issues](https://gitlab.com/pyladiesblr/pyladiesblr/issues), including closed issues. 
1. Create a new git branch specific to your change (as opposed to making commits in the master branch)
1. Create and switch to a new branch to write your feature or bugfix `git checkout -b new-branch-name` (replace new-branch-name with an appropriate name)
1. Create a Pull Request to merge the code with master branch
1. Once the Pull Request is approved by one of the reviewers, the code will be merged to the master branch

Note: Please make sure to always pull the latest code from master branch to avoid code conflicts.

**Please do not commit the generated/build files**